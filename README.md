# Exercício de Git - Curso de Cloudera

Bem-vindo ao exercício de Git do curso de Cloudera! Este exercício foi desenvolvido para ajudar você a praticar os conceitos básicos de Git, incluindo criação de repositórios, commits, branches e fusões. 

## Objetivo

O objetivo deste exercício é familiarizar você com as operações básicas do Git e preparar você para utilizar o Git em projetos reais, especialmente no contexto do Cloudera.

## Pré-requisitos

Antes de começar este exercício, certifique-se de que você possui o seguinte:
- Um editor de texto ou IDE de sua escolha (VSCode, Sublime Text, IntelliJ, etc.).
- Acesso a um terminal ou linha de comando.

## Passos do Exercício

### 1. Configuração Inicial


1. Crie um novo diretório para seu projeto e navegue até ele:
    ```bash
    mkdir cloudera-git-exercicio
    cd cloudera-git-exercicio
    ```

2. Inicialize um novo repositório Git:
    ```bash
    git init
    ```

### 2. Adicionando Arquivos e Criando Commits

1. Crie um novo arquivo chamado `README.md` e adicione algumas informações básicas sobre o projeto:
    ```markdown
    # Projeto Cloudera

    Este é um projeto de exemplo para praticar comandos básicos de Git.
    ```

2. Adicione o arquivo ao índice (staging area):
    ```bash
    git add README.md
    ```

3. Crie um commit com uma mensagem descritiva:
    ```bash
    git commit -m "Adiciona README.md com informações iniciais"
    ```

### 3. Criando e Trabalhando com Branches

1. Crie uma nova branch chamada `feature-branch`:
    ```bash
    git checkout -b feature-branch
    ```

2. Na `feature-branch`, crie um novo arquivo chamado `hello_world.py` e adicione o seguinte conteúdo:
    ```python
    print("Olá, Mundo!")
    ```

3. Adicione e faça commit das alterações:
    ```bash
    git add hello_world.py
    git commit -m "Adiciona hello_world.py com script de exemplo"
    ```

4. Mude de volta para a branch `main`:
    ```bash
    git checkout main
    ```

5. Mescle a `feature-branch` na `main`:
    ```bash
    git merge feature-branch
    ```

### 4. Visualizando o Histórico e Diferenças

1. Visualize o histórico de commits:
    ```bash
    git log
    ```

2. Visualize as diferenças entre dois commits:
    ```bash
    git diff <commit1> <commit2>
    ```

### 5. Trabalhando com Repositórios Remotos

1. Crie um repositório no GitHub, GitLab ou Bitbucket.

2. Adicione o repositório remoto ao seu projeto local:
    ```bash
    git remote add origin <url_do_repositorio>
    ```

3. Envie as alterações para o repositório remoto:
    ```bash
    git push -u origin main
    ```

### Recursos Adicionais

- [Documentação Oficial do Git](https://git-scm.com/doc)
- [Guia Interativo do Git](https://learngitbranching.js.org/)


### Como configurar sua própria IDE

1. Baixe o Visual Studio, escolha a versão de acordo com seu sistema operacional: https://code.visualstudio.com/download
2. Depois do Visual Studio baixado, instale as seguintes extensões (aba extensões no menu lateral): Python(Id: ms-python.python), Yaml(Id: redhat.vscode-yaml), Remote - SSH (Id: ms-vscode-remote.remote-ssh), Name: Remote - SSH: Editing Configuration Files (Id: ms-vscode-remote.remote-ssh-edit), Remote Explorer (Id: ms-vscode.remote-explorer)
3. Para o próximo passo lembre-se de ativar a VPN
4. Na aba lateral clique no Remote explorer e depois no + para abrir um comando ssh
6. O comando ssh que deve ser digitado faz referencia às informações que já foram utilizadas na configuração do MobaXterm, um exemplo padrão: ssh usuario@ip_da_maquina_remota -i caminho_do_arquivo
7. Para exemplificar um pouco mais, no meu caso fica: `ssh cloudera@10.1.0.5 -i C:\ssh\CDPMaster_key.pem`
8. Pronto você está com acesso no cluster usando sua própria IDE!


- [Documentação oficial do VScode sobre ssh-Remoto](https://code.visualstudio.com/docs/remote/ssh#_remember-hosts-and-advanced-settings)
- [Documentação oficial do VScode](https://code.visualstudio.com/docs/remote/ssh#_remember-hosts-and-advanced-settings)


## Conclusão

Parabéns! Você completou o exercício de Git. Agora você deve estar mais confortável com as operações básicas do Git e pronto para aplicar esse conhecimento em seus projetos do Cloudera. Se tiver alguma dúvida ou precisar de ajuda, não hesite em perguntar!

**Instrutor:** Yuri Silva
